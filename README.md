# Bookstore

main functionality:
1) write off the book
2) create an order
3) cancel the order
4) change the order status (new, completed, canceled)
5) add the book to the warehouse (closes all requests for the book and changes its status)
6) submit a request for a book
the program allows you to view:
1) list of books (sorted alphabetically, date , price, stock availability)
2) list of orders (sorted by date and price)
3) list of book requests (sorted by number of requests and alphabet)
4) list of completed orders for a period of time
5) the amount of money earned
6) number of completed orders
7) order review
8) viewing the book description