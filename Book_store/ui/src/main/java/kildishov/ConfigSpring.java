package senla;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@ComponentScan({"senla"})
@PropertySource(value = "my.properties", ignoreResourceNotFound = true)
public class ConfigSpring {
}
