package senla.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import senla.model.builder.Builder;
import senla.model.navigator.Navigator;
import senla.view.ViewController;

import java.util.Scanner;
import java.util.logging.Logger;



@Component
public class MenuController {

	private static Logger log = Logger.getLogger(MenuController.class.getName());
	
	@Autowired
	private Builder builder;
	
	@Autowired
	private Navigator navigator;

	@Autowired
	private ViewController viewController;

	public void run() {


		viewController.print("Welcome to the our store");

		try (Scanner scanner = new Scanner(System.in)) {
			while (true) {

				navigator.printMenu();

				Integer index = scanner.nextInt();

				try {

					navigator.navigate(index - 1);

				} catch (Exception e) {

					log.info(e.getMessage());

				}
			}
		}

	}

}
