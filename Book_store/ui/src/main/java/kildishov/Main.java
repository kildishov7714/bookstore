package senla;

import org.apache.log4j.BasicConfigurator;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import senla.controller.MenuController;

import java.io.IOException;



public class Main {

    public static void main(String[] args) throws SecurityException, IOException {

        BasicConfigurator.configure();

        ApplicationContext context = new AnnotationConfigApplicationContext(ConfigSpring.class);

        MenuController menuController =  context.getBean( MenuController.class);

        menuController.run();

    }

}
