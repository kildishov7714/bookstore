package senla.view;

import org.springframework.stereotype.Component;

import java.util.List;




@Component
public class ViewController {
	

	public void print(String text) {

		System.out.println(text);
	}

	public void printList(List<String> listText) {

		System.out.println(listText.toString());

	}


}
