package senla.model.navigator;


import senla.model.menu.Menu;

public interface INavigator {

	void setCurrentMenu(Menu buildMenu);

	void printMenu();

	void navigate(Integer index);

}