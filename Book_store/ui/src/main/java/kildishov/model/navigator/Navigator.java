package senla.model.navigator;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import senla.model.action.IAction;
import senla.model.builder.Builder;
import senla.model.menu.Menu;
import senla.model.menu.MenuItem;

import senla.view.ViewController;

import javax.annotation.PostConstruct;
import java.util.Optional;
import java.util.stream.Collectors;



@Component
public class Navigator implements INavigator {

	private Menu currentMenu;

	@Autowired
	private Builder builder;

	@Autowired
	private ViewController viewController;

	public Menu getCurrentMenu() {
		return currentMenu;
	}

	@Override
	public void setCurrentMenu(Menu curerntMenu) {

		this.currentMenu = currentMenu;

	}

	@PostConstruct
	public void insertRootMenu() {

		currentMenu = builder.getRootMenu();

	}

	@Override
	public void navigate(Integer index) {

		MenuItem submenu = currentMenu.getItems().get(index);

		Optional.ofNullable(submenu.getAction()).ifPresent(IAction::execute);

		currentMenu = submenu.getNextMenu();

	}

	@Override
	public void printMenu() {

		viewController.print(currentMenu.getName() + ". Please choose: ");
		viewController.printList(currentMenu.getItems().stream().map(MenuItem::getTitle).collect(Collectors.toList()));

	}

}