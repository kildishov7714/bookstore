package senla.model.action.orderAction;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import senla.model.action.IAction;
import senla.service.OrderMove;


import java.util.Scanner;
import java.util.logging.Logger;


@Component
public class SortOrder implements IAction {

	private static Logger logger = Logger.getLogger(SortOrder.class.getName());

	@Autowired
	private OrderMove orderMove;



	@Override
	public void execute() {

		try {

			Scanner input = new Scanner(System.in);

			System.out.print("1 - Date; 2 - Price; 3 - Status; ");
			int id_switch = input.nextInt();
			switch (id_switch) {

				case 1:
					orderMove.listOrderByDate();

					break;
				case 2:
					orderMove.listOrderByPrice();

					break;
				case 3:
					orderMove.listOrderByStatus();

					break;

				default:
					System.out.println("something wrong !");

			}
		} catch (Exception e) {

			logger.info(e.getMessage());

		}




	}

}
