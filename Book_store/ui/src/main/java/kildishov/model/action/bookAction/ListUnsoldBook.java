package senla.model.action.bookAction;

import java.util.logging.Logger;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import senla.model.action.IAction;
import senla.service.BookMove;


@Component
public class ListUnsoldBook implements IAction {
	
	private static Logger logger = Logger.getLogger(ListUnsoldBook.class.getName());

	@Autowired
	private BookMove bookMove;


	@Override
	public void execute() {

		try {

			bookMove.getUnsoldList();

		} catch (Exception e) {

			logger.info(e.getMessage());

		}

	}

}
