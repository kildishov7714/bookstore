package senla.model.action.bookAction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import senla.model.action.IAction;
import senla.service.BookMove;


import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Scanner;
import java.util.logging.Logger;

@Component
public class CreateBook implements IAction {

    private static Logger logger = Logger.getLogger(AddBookToWearhouse.class.getName());
    @Autowired
    private BookMove bookMove;

    @Override
    public void execute(){

        try{

            Scanner inputPrice = new Scanner(System.in);
            System.out.print("input price : ");
            double price = inputPrice.nextDouble();

            Scanner inputNeme = new Scanner(System.in);
            System.out.print("input name : ");
            String name = inputNeme.nextLine();

            Scanner inputText = new Scanner(System.in);
            System.out.print("input book text : ");
            String bookText = inputNeme.nextLine();

            Scanner inputYear = new Scanner(System.in);
            System.out.print("input year   : ");
            int year = inputYear.nextInt();

            Scanner inputMonth = new Scanner(System.in);
            System.out.print("input month   : ");
            int month = inputMonth.nextInt();

            Scanner inputDay = new Scanner(System.in);
            System.out.print("input day   : ");
            int day = inputDay.nextInt();


            Calendar data = new GregorianCalendar(year, month, day);

            bookMove.createBook( price, name, bookText, data);

        }catch (Exception e) {

            logger.info(e.getMessage());

        }


    }
}
