package senla.model.action.orderAction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import senla.model.action.IAction;
import senla.service.OrderMove;


import java.util.logging.Logger;


@Component
public class CountCompletedOrder implements IAction {
	private static Logger logger = Logger.getLogger(CountCompletedOrder.class.getName());

	@Autowired
	private OrderMove orderMove;



	@Override
	public void execute() {

		try {
			orderMove.countCompletedOrder();
		} catch (Exception e) {

			logger.info(e.getMessage());

		}



	}


}
