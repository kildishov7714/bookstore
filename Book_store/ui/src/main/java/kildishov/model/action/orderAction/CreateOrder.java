package senla.model.action.orderAction;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import senla.model.Book;
import senla.model.action.IAction;
import senla.model.action.bookAction.AddBookToWearhouse;
import senla.service.BookMove;
import senla.service.OrderMove;


import java.util.*;
import java.util.logging.Logger;


@Component
public class CreateOrder implements IAction {


	private static Logger logger = Logger.getLogger(CreateOrder.class.getName());
	@Autowired
	private OrderMove orderMove;

	@Autowired
	private BookMove bookMove;


	@Override
	public void execute() {

		try {


			Scanner inputCustomerId = new Scanner(System.in);
			System.out.print("input Customer Id : ");
			int customerId = inputCustomerId.nextInt();

			Scanner inputYear = new Scanner(System.in);
			System.out.print("input year: ");
			int year = inputYear.nextInt();

			Scanner inputMonth = new Scanner(System.in);
			System.out.print("input month  : ");
			int month = inputMonth.nextInt();

			Scanner inputDay = new Scanner(System.in);
			System.out.print("input day   : ");
			int day = inputDay.nextInt();

			Calendar data = new GregorianCalendar(year, month, day);


			Map<Book, Integer> books = new HashMap<>();

			int count = 0;

			int choise = 1;
			do{
				Scanner inputIdBook = new Scanner(System.in);
				System.out.print("input Book Id : ");
				int idBook = inputIdBook.nextInt();

				Book book = bookMove.findBookById(idBook);

				Scanner inputCount = new Scanner(System.in);
				System.out.print("input count : ");
				count = inputCount.nextInt();

				books.put(book, count);

				Scanner inputChoise = new Scanner(System.in);
				System.out.print("Enter 0 if you want add book ");
				choise = inputChoise.nextInt();
			}
			while (choise == 0);

			double totalPrice = 0;

			for(Book book : books.keySet()) {

				totalPrice = totalPrice + (book.getPrice() * books.get(book) );

			}

			orderMove.createOrder( books,  customerId, data, totalPrice);


		} catch (Exception e) {

			logger.info(e.getMessage());

		}

	}
}
