package senla.model.action.bookAction;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import senla.model.action.IAction;
import senla.service.BookMove;


import java.util.Scanner;
import java.util.logging.Logger;
@Component
public class FindBookById  implements IAction {

    private static Logger logger = Logger.getLogger(AddBookToWearhouse.class.getName());
    @Autowired
    private BookMove bookMove;



    @Override
    public void execute() {


        try {
            Scanner input = new Scanner(System.in);
            System.out.print("input id   : ");
            int id = input.nextInt();
            System.out.println(bookMove.findBookById(id));

        }catch (Exception e) {

            logger.info(e.getMessage());

        }



    }

}
