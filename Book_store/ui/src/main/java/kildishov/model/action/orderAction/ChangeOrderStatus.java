package senla.model.action.orderAction;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import senla.model.Order;
import senla.model.action.IAction;
import senla.service.OrderMove;


import java.util.Scanner;
import java.util.logging.Logger;


@Component
public class ChangeOrderStatus implements IAction {

	private static Logger logger = Logger.getLogger(ChangeOrderStatus.class.getName());

	@Autowired
	private OrderMove orderMove;



	@Override
	public void execute() {
		try {
			Scanner input = new Scanner(System.in);
			System.out.print("input id order : ");
			int id = input.nextInt();
			System.out.print("1 - cancle; 2 - completed ");
			int id_switch = input.nextInt();
			switch (id_switch) {

				case 1:
					orderMove.cancelOrder(id);
					break;
				case 2:
					orderMove.completOrder(id);
					break;
				case 3:

				default:
					System.out.println("something wrong !");

			}

		} catch (Exception e) {

			logger.info(e.getMessage());

		}





	}

}
