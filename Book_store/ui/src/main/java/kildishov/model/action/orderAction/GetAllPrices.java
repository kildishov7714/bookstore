package senla.model.action.orderAction;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import senla.model.action.IAction;
import senla.service.OrderMove;


import java.util.logging.Logger;



@Component
public class GetAllPrices implements IAction {

	private static Logger logger = Logger.getLogger(GetAllPrices.class.getName());

	@Autowired
	private OrderMove orderMove;



	@Override
	public void execute() {

		orderMove.getAllPrices();

	}


}
