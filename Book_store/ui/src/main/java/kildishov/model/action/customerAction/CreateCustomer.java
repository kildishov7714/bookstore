package senla.model.action.customerAction;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import senla.model.Customer;
import senla.model.action.IAction;
import senla.model.action.bookAction.AddBookToWearhouse;
import senla.service.BookMove;
import senla.service.CustomerMove;


import java.util.Scanner;
import java.util.logging.Logger;

@Component
public class CreateCustomer implements IAction {

    private static Logger logger = Logger.getLogger(AddBookToWearhouse.class.getName());
    @Autowired
    private CustomerMove customerMove;

    @Override
    public void execute(){

        Scanner inputName = new Scanner(System.in);
        System.out.print("input customer name");
        String name = inputName.nextLine();

        customerMove.createCustomer(name);
    }



}
