package senla.model.action.orderAction;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import senla.model.action.IAction;
import senla.service.OrderMove;


import java.util.Scanner;
import java.util.logging.Logger;


@Component
public class LookAtInformationAboutOrder implements IAction {
	private static Logger logger = Logger.getLogger(LookAtInformationAboutOrder.class.getName());

	@Autowired
	private OrderMove orderMove;



	@Override
	public void execute() {



		try {

			Scanner input = new Scanner(System.in);
			System.out.print("input id   : ");
			int id = input.nextInt();
			orderMove.lookAtInformationAboutOrder(id);

		}catch (Exception e) {

			logger.info(e.getMessage());

		}


	}


}
