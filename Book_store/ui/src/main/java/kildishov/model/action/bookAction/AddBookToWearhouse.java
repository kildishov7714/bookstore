package senla.model.action.bookAction;

import java.util.Scanner;
import java.util.logging.Logger;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import senla.model.action.IAction;
import senla.service.BookMove;


@Component
public class AddBookToWearhouse implements IAction {
	
	private static Logger logger = Logger.getLogger(AddBookToWearhouse.class.getName());
	@Autowired
	private BookMove bookMove;

	@Override
	public void execute() {
		
		
	try {	
		Scanner input = new Scanner(System.in);
		System.out.print("input id   : ");
		int id = input.nextInt();
		bookMove.addToWarehouse(id);
		
	}catch (Exception e) {
		
		logger.info(e.getMessage());
		
	}
		
		 

	}

}
