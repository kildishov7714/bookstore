package senla.model.action.bookAction;

import java.util.Scanner;
import java.util.logging.Logger;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import senla.model.action.IAction;
import senla.service.BookMove;


@Component
public class LookAtInformationAboutBook implements IAction {

	@Autowired
	private BookMove bookMove;


	
	private static Logger logger = Logger.getLogger(LookAtInformationAboutBook.class.getName());

	@Override
	public void execute() {

		
		try {
			Scanner input = new Scanner(System.in);
			System.out.print("input id   : ");
			int id = input.nextInt();
			bookMove.lookAtInformationAboutBook(id);
			
		}catch (Exception e) {

			logger.info(e.getMessage());

		}


	}
}
