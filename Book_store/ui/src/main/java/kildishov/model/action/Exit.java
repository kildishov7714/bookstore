package senla.model.action;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import senla.repository.book.BookDAO;
import senla.repository.customer.CustomerDAO;
import senla.repository.order.OrderDAO;



import java.util.logging.Logger;



@Component
public class Exit implements IAction {

	private static Logger logger = Logger.getLogger(Exit.class.getName());

	@Autowired
	private OrderDAO orderDAO;
	@Autowired
	private CustomerDAO customerDAO;
	@Autowired
	private BookDAO bookDAO;

	public BookDAO getBookData() {
		return bookDAO;
	}

	public void setBookData(BookDAO bookDAO) {
		this.bookDAO = bookDAO;
	}

	public OrderDAO getOrderData() {
		return orderDAO;
	}

	public void setOrderData(OrderDAO orderDAO) {
		this.orderDAO = orderDAO;
	}

	public CustomerDAO getCustomerData() {
		return customerDAO;
	}

	public void setCustomerData(CustomerDAO customerDAO) {
		this.customerDAO = customerDAO;
	}

	@Override
	public void execute() {



		System.exit(0);

	}

}