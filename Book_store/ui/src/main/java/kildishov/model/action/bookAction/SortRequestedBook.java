package senla.model.action.bookAction;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import senla.model.action.IAction;
import senla.service.BookMove;


import java.util.Scanner;
import java.util.logging.Logger;


@Component
public class SortRequestedBook implements IAction {

	private static Logger logger = Logger.getLogger(SortRequestedBook.class.getName());

	@Autowired
	private BookMove bookMove;


	@Override
	public void execute() {

		try {

			Scanner input = new Scanner(System.in);

			System.out.print("1 - Name; 2 - Count request; ");
			int id_switch = input.nextInt();
			switch (id_switch) {

				case 1:
					bookMove.listRequestBookByCountRequest();

					break;
				case 2:
					bookMove.listRequestBookByName();

					break;

				default:
					System.out.println("something wrong !");

			}

		} catch (Exception e) {

			logger.info(e.getMessage());


		}

	}
}
