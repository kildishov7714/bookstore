package senla.model.action.orderAction;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import senla.model.action.IAction;
import senla.service.OrderMove;

import java.util.Scanner;
import java.util.logging.Logger;


@Component
public class SortCompletedOrder implements IAction {

	private static Logger logger = Logger.getLogger(SortCompletedOrder.class.getName());

	@Autowired
	private OrderMove orderMove;



	@Override
	public void execute() {

		try {

			Scanner input = new Scanner(System.in);

			System.out.print("1 - Date; 2 - Price; ");
			int id_switch = input.nextInt();
			switch (id_switch) {

				case 1:
					orderMove.listCompletedOrderSortByDate();

					break;
				case 2:
					orderMove.listCompletedOrderSortByPrice();

					break;

				default:
					System.out.println("something wrong !");

			}
		}catch (Exception e) {

			logger.info(e.getMessage());

		}





	}


}
