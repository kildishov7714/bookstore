package senla.model.builder;

import java.util.ArrayList;
import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import senla.model.action.Exit;
import senla.model.action.IAction;
import senla.model.action.customerAction.CreateCustomer;
import senla.model.menu.Menu;
import senla.model.menu.MenuItem;

import javax.annotation.PostConstruct;


@Component
public class Builder implements IBuilder {

	private Menu rootMenu;

	private IAction action;

	public  Builder() {

		rootMenu = new Menu("Root menu", new ArrayList<>());

	}

	
	
	@Autowired
	SortBook sortBook ;

	@Autowired
	SortOrder sortOrder;

	@Autowired
	SortRequestedBook sortRequestedBook;

	@Autowired
	SortCompletedOrder sortCompletedOrder;

	@Autowired
	GetAllPrices getAllPrices;

	@Autowired
	CountCompletedOrder countCompletedOrder;

	@Autowired
	ListUnsoldBook listUnsoldBook;

	@Autowired
	LookAtInformationAboutOrder lookAtInformationAboutOrder;

	@Autowired
	LookAtInformationAboutBook lookAtInformationAboutBook;

	@Autowired
	RemoveBookFromWearhouse removeBookFromWearhouse;
	
	@Autowired
	CreateOrder createOrder;
	
	@Autowired
	ChangeOrderStatus changeOrderStatus;

	@Autowired
	AddBookToWearhouse addBookToWearhouse;

	@Autowired
	MakeBookRequest makeBookRequest;

	@Autowired
	FindBookById findBookById;

	@Autowired
	CreateBook createBook;

	@Autowired
	CreateCustomer createCustomer;
	
	@Autowired
	Exit exit;

	
	@Override
	@PostConstruct
	public Menu buildMenu() {

		MenuItem Book_Item_1 = new MenuItem("1: create Book ", rootMenu, createBook);
		MenuItem Book_Item_2 = new MenuItem("2: find by id", rootMenu,findBookById);
		MenuItem Book_Item_3 = new MenuItem("3: sort book ", rootMenu, sortBook);
		MenuItem Book_Item_4 = new MenuItem("4: list unsold book list ", rootMenu,  listUnsoldBook);
		MenuItem Book_Item_5 = new MenuItem("5: Remove book from wearhouse  ", rootMenu,  removeBookFromWearhouse);
		MenuItem Book_Item_6 = new MenuItem("6: Add book to wearhouse ", rootMenu,  addBookToWearhouse);
		MenuItem Book_Item_7 = new MenuItem("7: Make request  ", rootMenu,  makeBookRequest);
		MenuItem Book_Item_8 = new MenuItem("8: Find by id", rootMenu,findBookById);
		MenuItem Book_Item_9 = new MenuItem("9: exit ", rootMenu,exit);

		MenuItem Order_Item_1 = new MenuItem("1: Create Order ", rootMenu,  createOrder);
		MenuItem Order_Item_2 = new MenuItem("2: SortCompletedOrder ", rootMenu,  sortCompletedOrder);
		MenuItem Order_Item_3 = new MenuItem("3: GetAllPrices ", rootMenu,  getAllPrices);
		MenuItem Order_Item_4 = new MenuItem("4: Count Completed Order ", rootMenu,  countCompletedOrder);
		MenuItem Order_Item_5 = new MenuItem("5: Look at information about order ", rootMenu,  lookAtInformationAboutOrder);
		MenuItem Order_Item_6 = new MenuItem("6: Create Order ", rootMenu,  createOrder);
		MenuItem Order_Item_7 = new MenuItem("7: Change order status ", rootMenu,  changeOrderStatus);
		MenuItem Order_Item_8 = new MenuItem("8: exit ", rootMenu, exit);

		MenuItem Customer_Item_9 = new MenuItem("1: create customer ", rootMenu, createCustomer);
		MenuItem Customer_Item_10 = new MenuItem("8: exit ", rootMenu, exit);

		Menu firstMenu = new Menu("Order", Arrays.asList(Order_Item_1, Order_Item_2, Order_Item_3, Order_Item_4,
				Order_Item_5, Order_Item_6, Order_Item_7, Order_Item_8));
		Menu secondMenu = new Menu("Book", Arrays.asList(Book_Item_1, Book_Item_2, Book_Item_3, Book_Item_4,
				Book_Item_5, Book_Item_6, Book_Item_7, Book_Item_8, Book_Item_9));
		Menu thirdMenu = new Menu("Customer", Arrays.asList(Customer_Item_9, Customer_Item_10));



		rootMenu.addItem(new MenuItem(firstMenu.getName(), firstMenu, null));
		rootMenu.addItem(new MenuItem(secondMenu.getName(), secondMenu, null));
		rootMenu.addItem(new MenuItem(thirdMenu.getName(), thirdMenu, null));

		return rootMenu;

	}

	@Override
	public Menu getRootMenu() {

		return rootMenu;

	}

}
