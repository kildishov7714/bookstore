package senla.model.builder;

import senla.model.menu.Menu;


public interface IBuilder {

	Menu buildMenu();

	Menu getRootMenu();

}
