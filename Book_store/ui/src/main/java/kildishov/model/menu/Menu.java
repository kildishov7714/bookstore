package senla.model.menu;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;



public class Menu {

	private String name;

	private List<MenuItem> menuItems = new ArrayList<>();

	public Menu(String name, List<MenuItem> menuItems) {

		this.name = name;

		this.menuItems = menuItems;

	}

	public String getName() {

		return name;

	}

	public List<MenuItem> getItems() {

		return menuItems;

	}

	public void addItem(MenuItem menuItem) {
		if (menuItems == null) {

			this.menuItems = new ArrayList<>();

		}
		Optional.ofNullable(menuItems).ifPresent(items -> items.add(menuItem));
	}

}
