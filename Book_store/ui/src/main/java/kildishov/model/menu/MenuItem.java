package senla.model.menu;


import senla.model.action.IAction;

public class MenuItem {

	private String title;

	private IAction action;

	private Menu nextMenu;

	public MenuItem(String title, Menu nextMenu, IAction action) {

		this.title = title;

		this.action = action;

		this.nextMenu = nextMenu;
	}

	public Menu getNextMenu() {

		return nextMenu;

	}

	public String getTitle() {

		return title;

	}

	public void setTitle(String title) {

		this.title = title;

	}

	public IAction getAction() {

		return action;

	}

}
