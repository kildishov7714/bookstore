import org.junit.Rule;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.mockito.junit.jupiter.MockitoExtension;
import kildishov.model.Book;
import kildishov.model.Order;
import kildishov.model.SortType;
import kildishov.repository.order.OrderDAO;
import kildishov.service.OrderMove;

import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;


@ExtendWith(MockitoExtension.class)
public class OrderMoveTest {


    @Rule
    public MockitoRule rule = MockitoJUnit.rule();
    @InjectMocks
    public OrderMove orderMove;
    @Mock
    public OrderDAO orderDAO;

    public Order order;

    public Map<Book, Integer> books = new HashMap<>();

    public  Book book;



    @BeforeEach
    public void setUp(){

        order = new Order(books,2, new GregorianCalendar(1999, 6, 6), 0);

    }

    @Test
    public void deleteCustomerShouldDeleteCustomer(){

        //Arrange
        int num = 1;
        //Act
        orderMove.deleteOrder(num);
        //Assert
        verify(orderDAO, times(1)).delete(num);

    }

    @Test
    public void saveBookShouldSaveBook(){

        //Arrange
        int num = 1;
        //Act
        orderMove.saveOrder(order);
        //Assert
        verify(orderDAO, times(1)).saveOrder(order);

    }


    @Test
    public void cancleOrderShouldCancleOrder(){

        //Arrange
        int num = 1;
        //Act
        orderMove.cancelOrder(num);
        //Assert
        verify(orderDAO, times(1)).updateOrder(order);


    }

    @Test
    public void completeOrderShouldCompleteOrder(){

        //Arrange
        int num = 1;
        //Act
        orderMove.completOrder(num);
        //Assert
        verify(orderDAO, times(1)).updateOrder(order);


    }

    @Test
    public void findOrderByIdShouldFindById(){

        int count = 1;

        when(orderDAO.findById(count)).thenReturn(order);
        //Act
        orderMove.findOrderById(count);
        //Assert
        verify(orderDAO, times(1)).findById(anyInt());
    }

    @Test
    public void getDataOrderListShouldGetOrderList(){

        //Act
        orderMove.getDataOrderList(SortType.BYNAME);

        //Assert
        verify(orderDAO, times(1)).getDataOrderList(any(SortType.class));

    }
}
