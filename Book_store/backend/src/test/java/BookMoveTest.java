import org.junit.Rule;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.mockito.junit.jupiter.MockitoExtension;
import kildishov.model.Book;
import kildishov.model.SortType;
import kildishov.repository.book.BookDAO;
import kildishov.service.BookMove;

import java.util.GregorianCalendar;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class BookMoveTest {

    @Rule
    public MockitoRule rule = MockitoJUnit.rule();
    @InjectMocks
    public BookMove bookMove;
    @Mock
    public BookDAO bookDAO;

    public Book book;



    @BeforeEach
    public void setUp(){

        book = new Book(100, "Book_name", "Book text",new GregorianCalendar(1999, 6, 6));

        book.setId(1);

    }


    @Test
    public void findBookByIdShouldFindById(){

        int count = 1;

        when(bookDAO.findById(count)).thenReturn(book);
        //Act
        bookMove.findBookById(count);
        //Assert
        verify(bookDAO, times(1)).findById(anyInt());
    }

    @Test
    public void saveBookShouldSaveBook(){

        //Arrange
        int num = 1;
        //Act
        bookMove.saveBook(book);
        //Assert
        verify(bookDAO, times(1)).saveBook(book);
    }

    @Test
    public void deleteBookShouldDeleteBook(){

        //Arrange
        int num = 1;
        //Act
        bookMove.deleteBook(num);
        //Assert
        verify(bookDAO, times(1)).delete(num);
    }

    @Test
    public void getDataBookListShouldGetDataBookList(){

        //Act
        bookMove.getDataBookList(SortType.BYNAME);

        //Assert
        verify(bookDAO, times(1)).getDataBookList(any(SortType.class));


    }

    @Test
    public void getDataUnsoldListShouldGetDataUnsoldBookList(){

        //Act
        bookMove.getDataUnsoldList(SortType.BYNAME);

        //Assert
        verify(bookDAO, times(1)).getDataUnsoldList(any(SortType.class));


    }

    @Test
    public void getDataRequestListShouldGetDataRequestBookList(){

        //Act
        bookMove.getDataRequestList(SortType.BYNAME);

        //Assert
        verify(bookDAO, times(1)).getDataRequestList(any(SortType.class));


    }



    @Test
    public void addToWarehouseShouldAddToWarehouse(){

        int count = 1;

        when(bookDAO.findById(count)).thenReturn(book);

        doNothing().when(bookDAO).updateBook(book);
        bookMove.addToWarehouse(count);

        verify(bookDAO,  times(1)).findById(anyInt());

        assertThat(book.getAvailability(), equalTo(true));

    }

    @Test
    public  void makeRequestShouldmakeRequest(){

        int count = 1;

        when(bookDAO.findById(count)).thenReturn(book);


        bookMove.makeRequest(count);

        verify(bookDAO,  times(1)).findById(anyInt());

        assertThat(book.getAvailability(), equalTo(true));

    }


    @Test
    public  void removeBookFromWearhouseShouldRemoveBookFromWearhouse(){

        int count = 1;

        when(bookDAO.findById(count)).thenReturn(book);

        bookMove.removeBookFromWearhouse(count);

        verify(bookDAO,  times(1)).findById(anyInt());

        assertThat(book.getAvailability(), equalTo(false));

    }


}
