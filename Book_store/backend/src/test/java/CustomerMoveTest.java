import org.junit.Rule;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.mockito.junit.jupiter.MockitoExtension;
import kildishov.model.Customer;
import kildishov.repository.customer.CustomerDAO;
import kildishov.service.CustomerMove;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class CustomerMoveTest {


    @Rule
    public MockitoRule rule = MockitoJUnit.rule();
    @InjectMocks
    public CustomerMove customerMove;
    @Mock
    public CustomerDAO customerDAO;

    public Customer customer;



    @BeforeEach
    public void setUp(){

        customer = new Customer("Test", "test@mail.com","qwerty");

    }

    @Test
    public void deleteCustomerShouldDeleteCustomer(){

        //Arrange
        int num = 1;
        //Act
        customerMove.deleteCustomer(num);
        //Assert
        verify(customerDAO, times(1)).delete(num);
    }

    @Test
    public void findCustomerByIdShouldFindCustomerById(){

        int count = 1;

        when(customerDAO.findById(count)).thenReturn(customer);
        //Act
        customerMove.findCustomerById(count);
        //Assert
        verify(customerDAO, times(1)).findById(anyInt());
    }

    @Test
    public void saveBookShouldSaveBook(){

        //Arrange
        int num = 1;
        //Act
        customerMove.saveCustomer(customer);
        //Assert
        verify(customerDAO, times(1)).saveCustomer(customer);
    }

    @Test
    public void getDataCustomerListShouldGetCustomerBookList(){

        //Act
        customerMove.getDataCustomerList();

        //Assert
        verify(customerDAO, times(1)).getDataCustomerList();

    }


}
