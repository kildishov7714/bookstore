package kildishov;

import kildishov.model.Order;
import kildishov.model.Role;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.springframework.stereotype.Service;
import kildishov.model.Book;
import kildishov.model.Customer;

import javax.annotation.PostConstruct;

@Service
public class HibernateSession {

    private SessionFactory sessionFactory;

    public HibernateSession() {}

    @PostConstruct
    public void init(){

        try {

            Configuration configuration = new Configuration().configure();
            configuration.addAnnotatedClass(Book.class);
            configuration.addAnnotatedClass(Customer.class);
            configuration.addAnnotatedClass(Order.class);
            configuration.addAnnotatedClass(Role.class);

            StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties());
            sessionFactory = configuration.buildSessionFactory(builder.build());

        } catch (Exception e) {
            System.out.println("Exception !" + e);
        }
    }

    public SessionFactory getSessionFactory() {
        return this.sessionFactory;
    }
}
