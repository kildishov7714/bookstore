package kildishov.repository.order;


import kildishov.model.Order;
import kildishov.model.SortType;

import java.util.List;

interface IOrderData {

	List<Order> getDataOrderList(SortType sortType);
	
	List<Order> getDataCompletedOrderList(SortType sortType);

}
