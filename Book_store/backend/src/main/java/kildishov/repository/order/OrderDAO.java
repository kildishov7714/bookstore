package kildishov.repository.order;


import kildishov.model.Order;
import kildishov.repository.customer.CustomerDAO;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import kildishov.HibernateSession;
import kildishov.model.SortType;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

@Repository
public class OrderDAO implements IOrderData {

	private static Logger logger = Logger.getLogger(CustomerDAO.class.getName());


	@Autowired
	public HibernateSession hibernateSession;

	static List<Order> completedOrderList = new ArrayList<>();

	public Order findById(int id) {

		Session session = hibernateSession.getSessionFactory().openSession();
		Order order = session.get(Order.class, id);
		session.close();
		return order;

	}

	public void saveOrder(Order order) {
		Session session = hibernateSession.getSessionFactory().openSession();
		Transaction tx1 = session.beginTransaction();
		session.save(order);
		tx1.commit();
		session.close();
	}



	public void updateOrder(Order order) {
		Session session = hibernateSession.getSessionFactory().openSession();
		Transaction tx1 = session.beginTransaction();
		session.update(order);
		tx1.commit();
		session.close();
	}

	public void delete(int id) {
		Session session = hibernateSession.getSessionFactory().openSession();
		Transaction tx1 = session.beginTransaction();
		Order order = session.get(Order.class, id);
		session.delete(order);
		tx1.commit();
		session.close();
	}

	@Override
	public List<Order> getDataOrderList(SortType sortType) {


		Session session = hibernateSession.getSessionFactory().openSession();

		if(sortType.equals(SortType.BYSTATUS)){

			return (List<Order>)session.createQuery("From Order order by status").list();

		}else  if ( sortType.equals(SortType.BYPRICE)){

			return (List<Order>)session.createQuery("From Order order by price").list();

		}


		return  (List<Order>) session.createQuery("From Order").list();
	}

	@Override
	public List<Order> getDataCompletedOrderList(SortType sortType) {

		Session session = hibernateSession.getSessionFactory().openSession();

	  if ( sortType.equals(SortType.BYPRICE)){

			return (List<Order>)session.createQuery("From Order o where o.status = 'completed' order by price").list();

		}


		return  (List<Order>) session.createQuery("From Order o where o.status = 'completed' ").list();

	}


}
