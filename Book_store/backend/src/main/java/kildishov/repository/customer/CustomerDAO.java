package kildishov.repository.customer;


import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import kildishov.HibernateSession;
import kildishov.model.Customer;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.Optional;


@Repository
public class CustomerDAO implements ICustomerData {

	private static Logger logger = Logger.getLogger(CustomerDAO.class.getName());

	@Autowired
	public HibernateSession hibernateSession;




	public Customer findByУь(String username) {
		Session session = hibernateSession.getSessionFactory().openSession();
		Customer customer = session.get(Customer.class, username);
		session.close();
		return customer;
	}

	public Optional<Customer> findByEmail(String email) {
		Session session = hibernateSession.getSessionFactory().openSession();
		CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
		CriteriaQuery<Customer> criteriaQuery = criteriaBuilder.createQuery(Customer.class);
		Root<Customer> from = criteriaQuery.from(Customer.class);

		return Optional.ofNullable(session.
					createQuery(criteriaQuery.select(from).where(from.get("email").in(email)))
					.getSingleResult());


	}


	public Customer findById(int id) {
		Session session = hibernateSession.getSessionFactory().openSession();
		Customer customer = session.get(Customer.class, id);
		session.close();
		return customer;
	}

	public Customer saveCustomer(Customer customer) {
		Session session = hibernateSession.getSessionFactory().openSession();
		Transaction tx1 = session.beginTransaction();
		session.save(customer);
		tx1.commit();
		session.close();
		return customer;
	}




	public void update(Customer customer) {
		Session session = hibernateSession.getSessionFactory().openSession();
		Transaction tx1 = session.beginTransaction();
		session.update(customer);
		tx1.commit();
		session.close();
	}

	public void delete(int id) {
		Session session = hibernateSession.getSessionFactory().openSession();
		Transaction tx1 = session.beginTransaction();
		Customer customer = session.get(Customer.class, id);
		session.delete(customer);
		tx1.commit();
		session.close();
	}



	@Override
	public List<Customer> getDataCustomerList() {

		List<Customer> customerList = (List<Customer>)  hibernateSession.getSessionFactory().openSession().createQuery("From Customer").list();

		return customerList;
	}

}

