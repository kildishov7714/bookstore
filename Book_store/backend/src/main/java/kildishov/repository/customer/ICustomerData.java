package kildishov.repository.customer;


import kildishov.model.Customer;

import java.util.List;

interface ICustomerData {

	List<Customer> getDataCustomerList();

}
