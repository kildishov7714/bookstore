package kildishov.repository.role;


import kildishov.model.Role;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import kildishov.HibernateSession;
import kildishov.model.ERole;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

@Component
public class RoleDAO {

    private static Logger log = Logger.getLogger(RoleDAO.class.getName());

    @Autowired
    private HibernateSession hibernateSession;

    public Role findByName(ERole eRole) {

        Session session = hibernateSession.getSessionFactory().openSession();
        try (session) {
            CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
            CriteriaQuery<Role> criteriaQuery = criteriaBuilder.createQuery(Role.class);
            Root<Role> from = criteriaQuery.from(Role.class);

            Role role = session.createQuery(criteriaQuery.select(from).where(from.get("name").in(eRole))).getSingleResult();
            if (role == null) {
                throw new RuntimeException();
            }
            return role;
        } catch (RuntimeException e) {
            RuntimeException runtimeException = new RuntimeException("Error, Role "+ eRole.name() +" is not found");
            log.error(runtimeException.getMessage());
            throw runtimeException;
        }
    }

    }



