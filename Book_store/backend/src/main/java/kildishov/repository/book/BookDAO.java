package kildishov.repository.book;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import kildishov.HibernateSession;
import kildishov.model.Book;
import kildishov.model.SortType;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

@Repository
public class BookDAO implements IBookData {

		static List<Book> requestBookList = new ArrayList<>();

		static List<Book> unsoldBookList = new ArrayList<>();

		@Value("${month:300}")
		private int month;
	
		@Autowired
		public HibernateSession hibernateSession;


		private static Logger logger = Logger.getLogger(BookDAO.class.getName());




		public Book findById(int id) {

			Session session = hibernateSession.getSessionFactory().openSession();
			Book book = session.get(Book.class, id);
			session.close();
			return book;
		}

		public void saveBook(Book book) {
			Session session = hibernateSession.getSessionFactory().openSession();
			Transaction tx1 = session.beginTransaction();
			session.save(book);
			tx1.commit();
			session.close();
		}



		public void updateBook(Book book) {
			Session session = hibernateSession.getSessionFactory().openSession();
			Transaction tx1 = session.beginTransaction();
			session.update(book);
			tx1.commit();
			session.close();
		}

		public void delete(int id) {
			Session session = hibernateSession.getSessionFactory().openSession();
			Transaction tx1 = session.beginTransaction();
			Book book = session.get(Book.class, id);
			session.delete(book);
			tx1.commit();
			session.close();
		}

		@Override
	 	public List<Book> getDataRequestList(SortType sortType) {

			Session session = hibernateSession.getSessionFactory().openSession();

			if(sortType.equals(SortType.BYNAME)){

				return (List<Book>)session.createQuery("From Book b where  b.countRequestBook > 0 order by name").list();

			}else  if ( sortType.equals(SortType.BYDATE)){

				return (List<Book>)session.createQuery("From Book b where  b.countRequestBook > 0 order by date").list();

			}else  if (sortType.equals(SortType.BYPRICE)){

				return (List<Book>)session.createQuery("From Book b where  b.countRequestBook > 0 order by price").list();
			}


			return  (List<Book>) session.createQuery("From Book b where  b.countRequestBook > 0").list();

		}

		@Override
		public List<Book>  getDataUnsoldList(SortType sortType) {

			Session session = hibernateSession.getSessionFactory().openSession();

			if(sortType.equals(SortType.BYNAME)){

				return (List<Book>)session.createQuery("From Book b where  b.date < (current_date - 200000) order by name").list();

			}else  if ( sortType.equals(SortType.BYDATE)){

				return (List<Book>)session.createQuery("From Book b where  b.date < (current_date - 200000) order by date").list();

			}else  if (sortType.equals(SortType.BYPRICE)){

				return (List<Book>)session.createQuery("From Book b where  b.date < (current_date - 200000) order by price").list();
			}


			return  (List<Book>) session.createQuery("From Book b where  b.date < (current_date - 200000)").list();


		}

		@Override
		public List<Book> getDataBookList(SortType sortType) {

			
			Session session = hibernateSession.getSessionFactory().openSession();

			if(sortType.equals(SortType.BYNAME)){

				return (List<Book>)session.createQuery("From Book order by name").list();

			}else  if ( sortType.equals(SortType.BYDATE)){

				return (List<Book>)session.createQuery("From Book order by date").list();

			}else  if (sortType.equals(SortType.BYPRICE)){

				return (List<Book>)session.createQuery("From Book order by price").list();
			}


			return  (List<Book>) session.createQuery("From Book").list();

		}

}
