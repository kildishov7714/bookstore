package kildishov.repository.book;


import kildishov.model.Book;
import kildishov.model.SortType;

import java.util.List;

interface IBookData {
		
	List<Book> getDataBookList(SortType sortType);
	
	List<Book> getDataRequestList(SortType sortType);
	
	List<Book> getDataUnsoldList(SortType sortType);


}
