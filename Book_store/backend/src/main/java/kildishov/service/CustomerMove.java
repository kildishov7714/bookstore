package kildishov.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import kildishov.model.Customer;
import kildishov.repository.customer.CustomerDAO;

import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;

@Service
public class CustomerMove {

    private static Logger log = Logger.getLogger(BookMove.class.getName());

    @Autowired
    public CustomerDAO customerDAO;
    //
    public  void deleteCustomer(int id){

        customerDAO.delete(id);

    }

    public Customer findCustomerById(int id) {

        return customerDAO.findById(id);

    }

    public Customer saveCustomer(Customer customer){

        return customerDAO.saveCustomer(customer);

    }

    public List<Customer> getDataCustomerList(){

        return customerDAO.getDataCustomerList();

    }


    public Optional<Customer> findCustomerByEmail(String email) {

        return customerDAO.findByEmail(email);

    }

}
