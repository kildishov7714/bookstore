package kildishov.service;


import com.sun.istack.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import kildishov.model.Book;
import kildishov.model.Order;
import kildishov.model.OrderStatus;
import kildishov.model.SortType;
import kildishov.repository.book.BookDAO;
import kildishov.repository.order.OrderDAO;

import java.util.List;

@Service
public class OrderMove {

    private static Logger log = Logger.getLogger(OrderMove.class);

    @Autowired
    public OrderDAO orderDAO;

    @Autowired
    public BookDAO bookDAO;


    public void deleteOrder(int id){

        orderDAO.delete(id);

    }

    public void saveOrder(Order order){

        orderDAO.saveOrder(order);

    }

     public void getAllPrices(){

         bookDAO.getDataBookList(SortType.BYNAME);


         for (Order order : orderDAO.getDataOrderList(SortType.BYNAME)) {

             double totalPrice = 0;

             for(Book book : order.getBooks().keySet()) {

                 totalPrice = totalPrice + (book.getPrice() * order.getBooks().get(book) );

             }


             order.setPrice(totalPrice);


             orderDAO.updateOrder(order);

         }

     }

     public void cancelOrder(int id){

         Order order = orderDAO.findById(id);

         order.setStatus(OrderStatus.cancelled);

         orderDAO.updateOrder(order);

     }

    public void completOrder(int id){

         Order order = orderDAO.findById(id);

         order.setStatus(OrderStatus.completed);

         orderDAO.updateOrder(order);

    }


    public Order findOrderById(int id) {

      return orderDAO.findById(id);


    }

    public List<Order> getDataCompletedOrderList(SortType sortType) {

       return orderDAO.getDataCompletedOrderList(sortType);

    }

    public List<Order> getDataOrderList(SortType sortType) {

        return orderDAO.getDataOrderList(sortType);

    }





}
