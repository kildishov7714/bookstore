package kildishov.service;


import kildishov.HibernateSession;
import kildishov.repository.book.BookDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import kildishov.model.Book;
import kildishov.model.SortType;

import java.util.List;
import java.util.logging.Logger;
@Service
public class BookMove {

    private static Logger log = Logger.getLogger(BookMove.class.getName());

    @Autowired
    public BookDAO bookDAO;
    @Autowired
    private HibernateSession hibernateSession;

    //
    public Book findBookById(int id) {

        return bookDAO.findById(id);

    }
//
    public void saveBook(Book book){

        bookDAO.saveBook(book);

    }
    //
    public void deleteBook(int id){

        bookDAO.delete(id);

    }

//
    public List<Book> getDataBookList(SortType sortType){

        return bookDAO.getDataBookList(sortType );

    }
    //
    public List<Book> getDataUnsoldList(SortType sortType){

        return bookDAO.getDataUnsoldList(sortType );

    }

//
    public List<Book> getDataRequestList(SortType sortType){

        return bookDAO.getDataRequestList(sortType );

    }

//
    public void addToWarehouse(int id) {

        Book book = bookDAO.findById(id);

        book.setAvailability(true);

        bookDAO.updateBook(book);


    }


//
    public void makeRequest(int id) {

        Book book = bookDAO.findById(id);

        int count = book.getCountRequestBook();

        count++;

        book.setCountRequestBook(count);

        if (book.getAvailability() == false) {

            book.setAvailability(true);

            bookDAO.updateBook(book);

                }
            }

    //
    public void removeBookFromWearhouse(int id) {

        Book book = bookDAO.findById(id);

        book.setAvailability(false);

        bookDAO.updateBook(book);

    }

}
