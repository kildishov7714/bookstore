package kildishov.model;

import javax.persistence.*;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Comparator;


@Entity
@Table(name = "book")
public class Book implements Comparable<Book>, Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false)
	private int id; 
	@Column(name = "price")
	private double price;
	@Column(name = "name")
	private String name;
	@Column(name = "book_text")
	private String bookText;
	@Column(name = "availability")
	private boolean availability;
	@Column(name = "date")
	private Calendar date;
	@Column(name = "count_request_book")
	private int countRequestBook;
	@Column(name = "unsold_book")
	private boolean unsoldBook;


    public Book() {
		
	}

	public Book( double price, String name, String bookText, Calendar data) {

		this.price = price;

		this.name = name;

		this.bookText = bookText;

		this.date = data;

	}

	public int getCountRequestBook() {
		return countRequestBook;
	}

	public void setCountRequestBook(int countRequestBook) {
		this.countRequestBook = countRequestBook;
	}

	public boolean unsoldBook() {
		return unsoldBook;
	}

	public void setUnsoldBook(boolean unsoldBook) {
		this.unsoldBook = unsoldBook;
	}

	public Calendar getDate() {
		return date;
	}

	public void setDate(Calendar data) {
		this.date = data;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBookText() {
		return bookText;
	}

	public void setBookText(String bookText) {
		this.bookText = bookText;
	}

	public boolean getAvailability() {
		return availability;
	}

	public void setAvailability(boolean availability) {
		this.availability = availability;
	}

	@Override

	public String toString() {

		return id + " " + price + " " + name + " " + new SimpleDateFormat("dd.MM.yyyy").format(getDate().getTime())
				+ " " + availability + "\n";
	}


	public static Comparator<Book> PriceComparator = new Comparator<Book>() {

		@Override
		public int compare(Book book_1, Book book_2) {

			return (int) (book_1.getPrice() - book_2.getPrice());

		}
	};

	public static Comparator<Book> NameComparator = new Comparator<Book>() {

		@Override
		public int compare(Book Book_1, Book Book_2) {

			return Book_1.getName().compareTo(Book_2.getName());

		}
	};

	public static Comparator<Book> TimeComparator = new Comparator<Book>() {

		@Override
		public int compare(Book book_1, Book book_2) {

			return (int) book_1.getDate().compareTo(book_2.getDate());

		}
	};

	public static Comparator<Book> AvailabilityComparator = new Comparator<Book>() {

		@Override
		public int compare(Book Book_1, Book Book_2) {

			return Boolean.compare(Book_1.getAvailability(), Book_2.getAvailability());

		}

	};

	public static Comparator<Book> CountRequestBookComparator = new Comparator<Book>() {

		@Override
		public int compare(Book book_1, Book book_2) {

			return Integer.compare(book_1.getCountRequestBook(), book_2.getCountRequestBook());

		}
	};




    @Override
	public int compareTo(Book o) {

		return 0;
	}

}
