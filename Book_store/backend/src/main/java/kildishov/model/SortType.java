package kildishov.model;

public enum SortType {

    BYNAME,
    BYDATE,
    BYPRICE,
    BYCOUNTREQUEST,
    BYSTATUS,

}
