package kildishov.model;

import javax.persistence.*;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.*;

@Entity
@Table(name = "orders")
public class Order implements Comparable<Order>, Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false)
	private int id;
	@Column(name = "customer_id")
	private int customerId;
	@Column(name = "status")
	@Enumerated(EnumType.STRING)
	private OrderStatus status;
	@Column(name = "count")
	@ElementCollection(fetch = FetchType.EAGER)
	@CollectionTable(name = "hash_order", joinColumns = {
			@JoinColumn(name = "order_id", referencedColumnName = "id")})
	@MapKeyColumn(name = "oder_id")
		private Map<Book, Integer> books = new HashMap<>();
	@Column(name = "data")
	private Calendar data;
	@Column(name = "price", nullable = true)
	private double price;

	public Order() {

	}

	public Order(Map<Book, Integer> books, int customerId, Calendar data, double price) {

		this.books = books;

		this.customerId = customerId;

		this.setStatus(OrderStatus.New);

		this.data = data;

		this.price = price;

	}





	public void setBooks(Map<Book, Integer> books) {
		this.books = books;
	}

	public Map<Book, Integer> getBooks() {
		return books;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	public OrderStatus getStatus() {
		return status;
	}

	public void setStatus(OrderStatus status) {
		this.status = status;
	}

	public Calendar getData() {
		return data;
	}

	public void setData(Calendar data) {
		this.data = data;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	@Override
	public String toString() {
		return id + "," + status + ", " + new SimpleDateFormat("dd.MM.yyyy").format(getData().getTime()) + "\n";
	}

	public static Comparator<Order> TimeComparator = new Comparator<Order>() {

		@Override
		public int compare(Order order_1, Order order_2) {

			return (int) order_1.getData().compareTo(order_2.getData());

		}
	};

	public static Comparator<Order> StatusComparator = new Comparator<Order>() {

		@Override
		public int compare(Order order_1, Order order_2) {

			return (int) order_1.getStatus().compareTo(order_2.getStatus());

		}
	};



	public static Comparator<Order> PriceComparator = new Comparator<Order>() {

		@Override
		public int compare(Order order_1, Order order_2) {

			return (int) Double.compare(order_1.getPrice(), order_2.getPrice());

		}
	};



	@Override
	public int compareTo(Order o) {

		return 0;
	}

}
