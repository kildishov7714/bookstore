package com.senla.bookstore.dependency;

import com.senla.bookstore.dependency.annotation.Autowired;
import com.senla.bookstore.dependency.annotation.InjectProperty;
import com.senla.bookstore.dependency.annotation.PostConstruct;
import com.senla.bookstore.dependency.stereotype.Component;
import com.senla.bookstore.dependency.stereotype.Service;
import com.senla.bookstore.utils.Config;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

@Component
public class BeanFactory {

	private Map<String, Object> singletons = new HashMap();

	public Object getBean(String beanName) {

		return singletons.get(beanName);

	}

	public Map<String, Object> getSingletons() {
		return singletons;
	}

	public void init() {

		String basePackage = "com";

		instantiate(basePackage);

		initProperty();

		populateProperties();

		invokeInit();

	}

	public void instantiate(String basePackage) {

		try {

			ClassLoader classLoader = ClassLoader.getSystemClassLoader();

			String path = basePackage.replace('.', '/');

			Enumeration<URL> resources = classLoader.getResources(path);

			while (resources.hasMoreElements()) {

				URL resource = resources.nextElement();


				System.out.println(resource.toURI());


					File file = new File(resource.toURI());


					for (File classFile : file.listFiles()) {

						String fileName = classFile.getName();

						String fileNameNew = basePackage + "." + fileName;

						if (fileNameNew.endsWith(".class")) {

							String className = fileNameNew.substring(0, fileNameNew.length() - 6);

							Class classObject = Class.forName(className);

							if (classObject.isAnnotationPresent(Component.class)
									|| classObject.isAnnotationPresent(Service.class)) {

								Object instance = classObject.newInstance();

								String beanName = createBeanName(className);

								System.out.println(beanName);

								singletons.put(beanName, instance);


							}

						}

						instantiate(fileNameNew);

					}
				}


		} catch (NullPointerException | IOException | URISyntaxException | ClassNotFoundException
				| IllegalAccessException | InstantiationException | IllegalArgumentException e) {
			e.printStackTrace();

		}

	}

	public void populateProperties() {
		// System.out.println("==populateProperties==");
		try {
			for (Object object : singletons.values()) {
				for (Field field : object.getClass().getDeclaredFields()) {
					if (field.isAnnotationPresent(Autowired.class)) {

						for (Object dependency : singletons.values()) {
							if (dependency.getClass().equals(field.getType())) {
								field.setAccessible(true);
								field.set(object, dependency);

								/*
								 * String setterName = "set" + field.getName().substring(0, 1).toUpperCase() +
								 * field.getName().substring(1);// setPromotionsService
								 * System.out.println("Setter name = " + setterName); Method setter =
								 * object.getClass().getMethod(setterName, dependency.getClass());
								 * setter.invoke(object, dependency);
								 */

							}
						}

					}
				}
			}
		} catch (Exception e) {

			e.printStackTrace();

		}

	}

	public String createBeanName(String className) {

		String newCalssName;

		int index = className.indexOf(".");

		if (index == -1) {

			return className;

		} else {

			newCalssName = className.substring(index + 1);

		}

		className = newCalssName;

		return createBeanName(className);
	}

	public void invokeInit() {

		try {
			for (Object object : singletons.values()) {
				for (Method method : object.getClass().getDeclaredMethods()) {
					if (method.isAnnotationPresent(PostConstruct.class)) {

						method.invoke(object);

					}
				}

			}
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (IllegalAccessException illegalAccessException) {
			illegalAccessException.printStackTrace();
		}
	}

	public void initProperty() {

		Config config = new Config();

		try {
			for (Object object : singletons.values()) {
				for (Field field : object.getClass().getDeclaredFields()) {
					if (field.isAnnotationPresent(InjectProperty.class)) {

						field.setAccessible(true);

						System.out.println(field.getName());

						if (field.getName().equals("month")) {

							field.set(object, config.getMonths());

							System.out.println(config.getMonths());

						}

						if (field.getName().equals("change")) {

							field.set(object, config.getAvailability());

							System.out.println(config.getAvailability());

						}

					}
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
