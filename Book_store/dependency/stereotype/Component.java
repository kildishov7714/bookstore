package com.senla.bookstore.dependency.stereotype;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;


@Retention(RetentionPolicy.RUNTIME)
public @interface Component {
	 String type() default "null";
}
