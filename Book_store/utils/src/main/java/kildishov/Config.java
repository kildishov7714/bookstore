package kildishov;



import java.io.FileInputStream;
import java.util.Properties;

import java.util.logging.Logger;

public class Config {

	private String propertiesFiel = "D:\\!SENLA_Courses\\Book_store\\src\\main\\resources\\my.properties";

	private Properties properties = new Properties();
	
	private static Logger logger = Logger.getLogger(Config.class.getName());

	public Config() {

		try {

			FileInputStream file = new FileInputStream(propertiesFiel);

			properties.load(file);

		} catch (Exception ex) {

			logger.info(ex.getMessage());

		}

	}
	
	
	public int getMonths() {

		return Integer.parseInt(properties.getProperty("month", "180"));
	}
	
	public boolean getAvailability() {
		
		return Boolean.parseBoolean(properties.getProperty("change", "true"));

	}

}
