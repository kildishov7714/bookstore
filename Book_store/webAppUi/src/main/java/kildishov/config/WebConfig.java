package kildishov.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@Configuration
@EnableWebMvc
@ComponentScan({"senla"})
@PropertySource(value = "my.properties", ignoreResourceNotFound = true)
public class WebConfig {
}
