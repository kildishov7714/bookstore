package kildishov.dto.order;

import kildishov.model.Order;
import kildishov.service.OrderMove;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import kildishov.model.Book;
import kildishov.service.BookMove;

import java.util.List;
import java.util.stream.Collectors;
@Component
public class ConvertOrder {

    @Autowired
    private BookMove bookMove;

    @Autowired
    private OrderMove orderMove;

    public  OrderDTO convertOrderToDTO(Order order){
        OrderDTO orderDTO = new OrderDTO();

        orderDTO.setId(order.getId());
        orderDTO.setCustomerId(order.getCustomerId());
        orderDTO.setBooks(order.getBooks().entrySet()
                .stream()
                .map(entry -> entry.getKey().toString() + ", amount= " + entry.getValue())
                .collect(Collectors.toList())) ;
        orderDTO.setData(order.getData());
        orderDTO.setPrice(order.getPrice());

        return orderDTO;
    }

    public Order convertDTOToOrder(OrderDTO orderDTO){

        Order order = new Order();

        order.setId(orderDTO.getId());
        order.setCustomerId(orderDTO.getCustomerId());
        order.setData(orderDTO.getData());
        order.setPrice(orderDTO.getPrice());


        for (String partOfList : orderDTO.getBooks()){
            int id = 0, amount = 0;
            String[] strings = partOfList.split(",");
            for (String partsOfString : strings){
                if (partsOfString.contains("id")){
                    id = Integer.parseInt(partsOfString.replaceAll("[^0-9]", ""));
                } else if (partsOfString.contains("amount") & id != 0){
                    amount = Integer.parseInt(partsOfString.replaceAll("[^0-9]", ""));
                    Book book = bookMove.findBookById(id);
                    order.getBooks().put(book, amount);
                }
            }
        }

        return  order;

    }

    public List<OrderDTO> listOfOrderConverter(List<Order> orderList){
        return orderList.stream().map(this::convertOrderToDTO).collect(Collectors.toList());
    }
}
