package kildishov.dto.book;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import kildishov.model.Book;
import kildishov.service.BookMove;

import java.util.List;
import java.util.stream.Collectors;
@Component
public class ConvertBook {

    @Autowired
    private BookMove bookMove;

    public BookDTO convertBookToDTO(Book book){

        BookDTO BookDTO = new BookDTO();

        BookDTO.setId(book.getId());
        BookDTO.setPrice(book.getPrice());
        BookDTO.setName(book.getName());
        BookDTO.setBookText(book.getBookText());
        BookDTO.setDate(book.getDate());

        return BookDTO;

    }

    public Book convertDTOToBook(BookDTO bookDTO){

        Book book = new Book();

        book.setId(bookDTO.getId());
        book.setPrice(bookDTO.getPrice());
        book.setName(bookDTO.getName());
        book.setBookText(bookDTO.getBookText());
        book.setDate(bookDTO.getDate());

        return book;
    }

    public List<BookDTO> listOfBookConverter(List<Book> bookList){
        return bookList.stream().map(this::convertBookToDTO).collect(Collectors.toList());
    }


}
