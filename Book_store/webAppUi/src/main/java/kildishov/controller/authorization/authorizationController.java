package kildishov.controller.authorization;


import kildishov.security.userDetails.UserDetailsImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import kildishov.jwt.JwtUtils;
import kildishov.model.Customer;
import kildishov.model.ERole;
import kildishov.model.Role;
import kildishov.repository.role.RoleDAO;
import kildishov.security.pojo.JwtResponse;
import kildishov.security.pojo.LoginRequest;
import kildishov.security.pojo.SignupRequest;
import kildishov.service.CustomerMove;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/auth")
@CrossOrigin(origins = "*", maxAge = 3600)
public class authorizationController {

    @Autowired
    AuthenticationManager authenticationManager;
    @Autowired
    PasswordEncoder passwordEncoder;
    @Autowired
    RoleDAO roleDAO;
    @Autowired
    CustomerMove customerMove;
    @Autowired
    JwtUtils jwtUtils;

    @PostMapping("/signin")
    @ResponseStatus(HttpStatus.OK)
    public JwtResponse authUser(@RequestBody LoginRequest loginRequest) {
        Authentication authentication = authenticationManager
                .authenticate(new UsernamePasswordAuthenticationToken(
                        loginRequest.getEmail(),
                        loginRequest.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtUtils.generateJwtToken(authentication);

        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        List<String> roles = userDetails.getAuthorities().stream()
                .map(item -> item.getAuthority())
                .collect(Collectors.toList());

        return new JwtResponse(jwt,
                userDetails.getId(),
                userDetails.getUsername(),
                roles);
    }


    @PostMapping("/signup")
    @ResponseStatus(HttpStatus.CREATED)
    public Customer registerUser(@RequestBody SignupRequest signupRequest) {

        Customer customer = new Customer(signupRequest.getUsername(),
                signupRequest.getEmail(),
                passwordEncoder.encode(signupRequest.getPassword()));
        Set<String> reqRoles = signupRequest.getRoles();
        Set<Role> roles = new HashSet<>();

        if (reqRoles == null) {
            Role userRole = roleDAO.findByName(ERole.ROLE_USER);
            System.out.println(userRole);
            roles.add(userRole);
        } else {
            reqRoles.forEach(r ->{
                String roleName = "ROLE_" + r.toUpperCase();

                ERole eRole = Arrays.stream(ERole.values())
                        .filter(role -> role.toString().equals(roleName))
                        .findFirst().orElse(ERole.ROLE_USER);
                Role userRole = roleDAO.findByName(eRole);
                roles.add(userRole);
            });
        }
        customer.setRoles(roles);
        return customerMove.saveCustomer(customer);
    }


}
