package kildishov.controller.customer;

import kildishov.service.CustomerMove;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import kildishov.model.Customer;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(path = "/customers", headers = "Accept=application/json")
public class CustomerController {

    @Autowired
    private CustomerMove customerMove;

    @GetMapping("/{id}")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public Customer findBookById(@PathVariable("id") int id){

        Customer customer = customerMove.findCustomerById(id);
        return customer;

    }

    @GetMapping("/{email}")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public Optional<Customer> findBookByEmail(@PathVariable("email") String email){

        Optional<Customer> customer = customerMove.findCustomerByEmail(email);
        return customer;

    }

    @PostMapping()
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public void createOrder(@RequestBody Customer customer) {

        customerMove.saveCustomer(customer);

    }



    @DeleteMapping(value = "/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public void delete(@PathVariable(name = "id") int id) {
        customerMove.deleteCustomer(id);
    }

    @GetMapping(value = "/getDataCustomerList")
    public List<Customer> getOrderList(){

        return customerMove.getDataCustomerList();

    }

}
