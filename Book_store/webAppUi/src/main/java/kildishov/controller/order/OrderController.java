package kildishov.controller.order;

import kildishov.model.Order;
import kildishov.service.OrderMove;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import kildishov.dto.order.ConvertOrder;
import kildishov.dto.order.OrderDTO;
import kildishov.model.SortType;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping(path ="/orders", headers = "Accept=application/json")
public class OrderController {

    @Autowired
    private ConvertOrder convertOrder;
    @Autowired
    private OrderMove orderMove;

    @GetMapping("/{id}")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public OrderDTO findBookById(@PathVariable("id") int id){

        OrderDTO orderDTO = convertOrder.convertOrderToDTO(orderMove.findOrderById(id));
        return orderDTO;

    }

    @PostMapping()
    @PreAuthorize("hasRole('ADMIN')")
    public void createOrder(@RequestBody OrderDTO orderDTO) {

        Order order = convertOrder.convertDTOToOrder(orderDTO);
        orderMove.saveOrder(order);

    }

    @DeleteMapping(value = "/{id}")
    @PreAuthorize(" hasRole('ADMIN')")
    public void delete(@PathVariable(name = "id") int id) {
        orderMove.deleteOrder(id);
    }

    @GetMapping(value = "/listOrder")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public List<OrderDTO> getDataCompletedOrderList(@RequestParam String sortType){

        SortType enumSortType = Arrays.stream(SortType.values())
                .filter(bookSortType -> bookSortType.toString().contains(sortType.toUpperCase()))
                .findFirst().get();

        return convertOrder.listOfOrderConverter(orderMove.getDataOrderList(enumSortType));

    }


    @GetMapping(value = "/listCompletedOrder")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public List<OrderDTO> getCompletedOrderList(@RequestParam String sortType){

        SortType enumSortType = Arrays.stream(SortType.values())
                .filter(bookSortType -> bookSortType.toString().contains(sortType.toUpperCase()))
                .findFirst().get();

        return convertOrder.listOfOrderConverter(orderMove.getDataCompletedOrderList(enumSortType));

    }

}
