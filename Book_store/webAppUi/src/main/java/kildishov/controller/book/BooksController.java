package kildishov.controller.book;

import kildishov.dto.book.BookDTO;
import kildishov.dto.book.ConvertBook;
import kildishov.service.BookMove;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import kildishov.model.Book;
import kildishov.model.SortType;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping(path ="/books", headers = "Accept=application/json")
public class BooksController {

    @Autowired
    private ConvertBook convertBook;
    @Autowired
    private BookMove bookMove;

    @GetMapping("/{id}")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public BookDTO findBookById(@PathVariable("id") int id){

        BookDTO bookDTO = convertBook.convertBookToDTO(bookMove.findBookById(id));
        return bookDTO;

    }

    @PreAuthorize("hasRole('ADMIN')")
    @DeleteMapping(value = "/{id}")
    public void delete(@PathVariable(name = "id") int id) {
        bookMove.deleteBook(id);
    }

    @PostMapping()
    @PreAuthorize("hasRole('ADMIN')")
    public void createBook(@RequestBody BookDTO bookDTO) {
        Book book = convertBook.convertDTOToBook(bookDTO);
        bookMove.saveBook(book);
    }


    @GetMapping(value = "/unsoldBookList")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public List<BookDTO> getUnsoldBookList( @RequestParam String sortType){

        SortType enumSortType = Arrays.stream(SortType.values())
                .filter(bookSortType -> bookSortType.toString().contains(sortType.toUpperCase()))
                .findFirst().get();

        return convertBook.listOfBookConverter(bookMove.getDataUnsoldList(SortType.BYNAME));
    }



    @GetMapping(value = "/listBook")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public List<BookDTO> getlistBook(@RequestParam String sortType){



        SortType enumSortType = Arrays.stream(SortType.values())
                .filter(bookSortType -> bookSortType.toString().contains(sortType.toUpperCase()))
                .findFirst().get();


        return convertBook.listOfBookConverter(bookMove.getDataBookList(enumSortType));

    }

    @GetMapping(value = "/listRequestBook")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public List<BookDTO> getDataRequestList(@RequestParam String sortType){

        SortType enumSortType = Arrays.stream(SortType.values())
                .filter(bookSortType -> bookSortType.toString().contains(sortType.toUpperCase()))
                .findFirst().get();

        return convertBook.listOfBookConverter(bookMove.getDataRequestList(enumSortType));

    }

}
